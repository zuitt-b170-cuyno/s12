// console.log("Hello World")

const firstName = "John", lastName = "Smith", age = 30, isMarried = true
const hobbies = ["Biking", "Mountain Climbing", "Swimming"]
const workAddress = {
    houseNumber: "32",
    street: "Washington",
    city: "Lincoln",
    state: "Nebraska"
}

console.log(`First Name: ${firstName}`)
console.log(`Last Name: ${lastName}`)
console.log(`Age: ${age}`)
console.log("Hobbies:")
console.log(hobbies)
console.log("Work Address:")
console.log(workAddress)

function printUserInfo(firstName, lastName, age) {
    console.log(`${firstName} ${lastName} is ${age} years of age`)
    console.log("This was printed inside the function")
    console.log(hobbies)
    console.log("This was printed inside the function")
    console.log(workAddress)
    console.log(`The value of isMarried is: ${isMarried}`)
}

printUserInfo(firstName, lastName, age)

function returnFunction(firstName, lastName, age) {
    return `My name is ${firstName} ${lastName} and I'm ${age}`
}

const introduction = returnFunction(firstName, lastName, age)