// one-line comment (ctrl + slash)
/* multi-line comment */

console.log("Hello, I'm B170 student!");

/* 
    Browser consoles are part of browsers which allow us to see/log messages, data, 
    or information from the programming languages.

    They are easily accessed in the dev tools.
    Statements - instruction, expressions that we add to the programming language to
        communicate with the computers.

    Usually ending with a semicolon. However, JS implemented a way to automatically add
    semicolons at the end of the line.

    Syntax - set of rules that describes how statements should be made/constructed.
    Line/block of codes must follow a certain set of rules for it to work properly.
*/

console.log("Johnny");
console.log("Dinuguan");
console.log("Sinigang na Baboy");
console.log("Liempo");

let food1 = "Adobo";
console.log(food1);

/* 
    Variable stores information or data within JS.
    To create a variable, we first declare the name of the variable with
    either let/const keyword.

    let <variable-name>
    We can initialize the variable with the value or data.

    let <variable-name> = <value>
*/

console.log("My favorite food is " + food1);
let food2 = "Kare-Kare"
console.log("My favorite foods are " + food1 + " and " + food2);

let food3;
console.log(food3);

/* 
    Update the content by reassigning the value using assignment operator (=)
    Assignment operator (=) lets us (re)assign values to a variable.
*/

food3 = "Chicken Joy";
console.log(food3);

// let food1 = "Flat Tops"
// console.log(food1)

const pi = 3.1416
// pi = "Pizza"

console.log(pi)
const gravity = 9.8
console.log(gravity)

food1 = "Chicken"
food2 = "Pork"
console.log(food1 + " and " + food2 + " are my favorite foods")

const sunriseDirection = "East", sunsetDirection = "West"
console.log(sunriseDirection + " and " + sunsetDirection + " are the sun's direction")

// Guidelines 

/* 
    String - data type which is alphanumeric text. This can be enclosed with 
    single (') or double (") quote.

*/

console.log("Sample String Data")

/* 
    Numerical data type
        - data types which are numeric. Displayed when numbers are not placed in the
          quotation marks. If there is mathematical operation needed to be done, 
          numerical data type should be used instead of string data type.
*/

console.log(123456789)
console.log(1 + 1)

console.log("Johnny Cuyno")
console.log("Nov 8, 1999")
console.log("Cavite")

console.log(100)
console.log(8)
console.log(3)