function add(x, y) {
    const sum = x + y
    console.log(sum)
}

add(5, 6)

function printBio(firstName, lastName, age) {
    console.log(`My name is ${firstName} ${lastName}`)
    console.log(`I'm ${age} years old`)
}

printBio("Johnny", "Cuyno", 22)

function addSum(x, y) {
    return x - y
}

addSum(10, 5)

function createFullName(fname, mname, lname) {
    return `${fname} ${mname} ${lname}`
}

const fullName = createFullName("Johnny", "Villamor", "Cuyno")
console.log(fullName)